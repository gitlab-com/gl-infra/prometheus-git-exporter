FROM alpine:3.6

RUN apk add --no-cache git openssh && \
  git config --global user.name "Prometheus Git Exporter" && \
  git config --global user.email "prometheus-git-exporter@gitlab.com" && \
  git config --global push.default matching && \
  mkdir /git /config /secrets ~/.ssh/ && \
  chown 0700 ~/.ssh/ && \
  ln -s /secrets/private_key ~/.ssh/id_rsa && \
  ssh-keyscan -H gitlab.com > ~/.ssh/known_hosts

COPY prometheus-git-exporter /

ENTRYPOINT /prometheus-git-exporter -config /config/prometheus-git-exporter.conf
