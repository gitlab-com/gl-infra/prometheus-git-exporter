package collector_test

import (
	"regexp"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gl-infra/prometheus-git-exporter/collector"
	"gitlab.com/gl-infra/prometheus-git-exporter/config"
	"gitlab.com/gl-infra/prometheus-git-exporter/git/testutil"
)

func TestCollectingMetrics(t *testing.T) {
	testRepo := testutil.NewTestRepo("testing-repo")
	defer testRepo.Destroy()

	col := collector.New(config.Config{
		Basepath:     testRepo.Basepath,
		Timeout:      99999,
		Housekeeping: 10,
		Repositories: []*config.Repository{
			&config.Repository{
				Name:      "testing-repo",
				RemoteURL: testRepo.Origin,
				Labels:    map[string]string{"env": "testing"},
			},
		},
	})

	ch := make(chan prometheus.Metric)
	defer close(ch)

	go col.Collect(ch)
	m := <-ch
	if ok, err := regexp.Match("git_pull_seconds", []byte(m.Desc().String())); !ok {
		t.Error("Returned metric is not git_pull_seconds as expected but ", m.Desc().String(), " error: ", err)
	}
	m = <-ch
	if ok, err := regexp.Match("git_push_seconds", []byte(m.Desc().String())); !ok {
		t.Error("Returned metric is not git_push_seconds as expected but ", m.Desc().String(), " error: ", err)
	}
	m = <-ch
	if ok, err := regexp.Match("git_repositories_configured_total", []byte(m.Desc().String())); !ok {
		t.Error("Returned metric is not git_repositories_configured_total as expected but ", m.Desc().String(), " error: ", err)
	}
	m = <-ch
	if ok, err := regexp.Match("git_scrape_total", []byte(m.Desc().String())); !ok {
		t.Error("Returned metric is not git_scrape_total as expected but ", m.Desc().String(), " error: ", err)
	}
	m = <-ch
	if ok, err := regexp.Match("git_scrape_duration_seconds", []byte(m.Desc().String())); !ok {
		t.Error("Returned metric is not git_scrape_duration_seconds as expected but ", m.Desc().String(), " error: ", err)
	}
}
