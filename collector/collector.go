package collector

import (
	"fmt"
	"os"
	"path"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/yudai/nmutex"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gl-infra/prometheus-git-exporter/config"
	"gitlab.com/gl-infra/prometheus-git-exporter/git"
	"gitlab.com/gl-infra/prometheus-git-exporter/housekeeping"
)

var (
	repoLocks          = nmutex.New()
	allowedOperations  = -1
	pullSecFQName      = prometheus.BuildFQName("git", "pull", "seconds")
	pushSecFQName      = prometheus.BuildFQName("git", "push", "seconds")
	failedScrapesCount = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "git",
		Subsystem: "scrape_timeouts",
		Name:      "total",
		Help:      "Total number of timed out scrapes",
	})
	repoCountGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "git",
		Subsystem: "repositories_configured",
		Name:      "total",
		Help:      "Total number of configured repositories",
	})
	repoScrapeHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
		Namespace: "git",
		Subsystem: "scrape_duration",
		Name:      "seconds",
		Help:      "Histogram of seconds used to scrape repositories",
	})
	repoScrapeCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "git",
		Subsystem: "scrape",
		Name:      "total",
		Help:      "Total number of repositories scraped so far",
	})
	housekeeper = housekeeping.New()
)

// GitCollector struct is used to capture all the metrics
type GitCollector struct {
	basepath          string
	timeout           int
	repositories      []*config.Repository
	allowedOperations int
}

// New creates a new Git Collector from the passed configuration
func New(c config.Config) GitCollector {
	housekeeper.Set(c.Housekeeping)
	for _, r := range c.Repositories {
		housekeeper.Add(r.Name)
	}
	if c.AllowedOperations > 0 {
		allowedOperations = c.AllowedOperations
	}
	return GitCollector{
		basepath:          c.Basepath,
		timeout:           c.Timeout,
		repositories:      c.Repositories,
		allowedOperations: allowedOperations,
	}
}

// Describe sends the metrics descriptions
func (g GitCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- prometheus.NewDesc(pullSecFQName, "Seconds it takes to pull from a repo", nil, nil)
	ch <- prometheus.NewDesc(pushSecFQName, "Seconds it takes to push to a repo", nil, nil)
	ch <- repoCountGauge.Desc()
	ch <- repoScrapeCounter.Desc()
	ch <- repoScrapeHistogram.Desc()
}

// Collect implents Prometheus Collector interface
func (g GitCollector) Collect(ch chan<- prometheus.Metric) {

	start := time.Now()
	defer func() {
		if err := recover(); err != nil {
			log.Errorf("Recovered execution from panic when collecting metrics in %f seconds: %s", time.Since(start).Seconds(), err)
		}
	}()

	wg := sync.WaitGroup{}
	wg.Add(len(g.repositories))
	repoCountGauge.Set(float64(len(g.repositories)))

	for _, repository := range g.repositories {
		go func(r config.Repository) {
			defer wg.Done()

			releaseLock := repoLocks.Lock(r.Name)
			defer releaseLock()

			repo := git.NewRepo(path.Join(g.basepath, r.Name), g.timeout, r.ResetCommits)
			if !repo.Local() {
				panic(fmt.Errorf("Repository path %s does not exists", repo.Path))
			}

			log.Debugf("Collecting metrics for repo %s", r.Name)

			if err := g.measure(ch, r, pullSecFQName, func() error {
				return repo.Pull()
			}); err != nil {
				log.Errorf("Failed to measure pulling from %s: %s", r.Name, err)
				return
			}

			if err := g.measure(ch, r, pushSecFQName, func() error {
				repo.Commit("Beep")
				return repo.Push()
			}); err != nil {
				log.Errorf("Failed to measure pushing to %s: %s", r.Name, err)
				return
			}

			if err := housekeeper.Run(r.Name, func() error {
				return repo.GC()
			}); err != nil {
				log.Errorf("Failed to gc %s: %s", r.Name, err)
				return
			}

			if err := housekeeper.Run(r.Name, func() error {
				return repo.Reset()
			}); err != nil {
				log.Errorf("Failed to reset %s: %s", r.Name, err)
				return
			}
			repoScrapeCounter.Inc()
		}(*repository)
	}

	wg.Wait()
	repoScrapeHistogram.Observe(time.Since(start).Seconds())

	ch <- repoCountGauge
	ch <- repoScrapeCounter
	ch <- repoScrapeHistogram
	ch <- failedScrapesCount

	err := g.terminate()
	if err != nil {
		log.Errorf("Failed to self-terminate! %s", err)
		return
	}
}

func (g *GitCollector) decrementOperations() {
	if g.allowedOperations > 0 {
		g.allowedOperations--
	}
}

func (g *GitCollector) terminate() error {
	g.decrementOperations()
	if g.allowedOperations != 0 {
		return nil
	}

	p, err := os.FindProcess(os.Getpid())
	if err != nil {
		return err
	}
	p.Signal(os.Interrupt)
	return nil
}

func (g *GitCollector) labels(r config.Repository) prometheusLabels {
	names := []string{"repository"}
	values := []string{r.Name}
	for key, value := range r.Labels {
		names = append(names, key)
		values = append(values, value)
	}
	return prometheusLabels{Names: names, Values: values}
}

func (g *GitCollector) measure(ch chan<- prometheus.Metric, r config.Repository,
	fqname string, f func() error) error {

	done := make(chan error, 1)

	labels := g.labels(r)

	start := time.Now()

	go func() {
		done <- f()
	}()

	printableLabels := make(map[string]string)
	for i := range labels.Names {
		printableLabels[labels.Names[i]] = labels.Values[i]
	}

	err := <-done
	elapsed := time.Since(start).Seconds()
	if err != nil {
		switch err.Error() {
		case "signal: killed":
			failedScrapesCount.Inc()
			return fmt.Errorf("context timeout of %d seconds exceeded for operation %s - labels: %v", g.timeout, fqname, printableLabels)
		default:
			ch <- prometheus.NewInvalidMetric(labels.Desc(fqname), err)
			return fmt.Errorf("failed to probe repository after %f seconds", elapsed)
		}
	}
	ch <- prometheus.MustNewConstMetric(labels.Desc(fqname), prometheus.GaugeValue, elapsed, labels.Values...)
	log.Debugf("Operation %s successful - labels: %v", fqname, printableLabels)
	return nil
}

type prometheusLabels struct {
	Names  []string
	Values []string
}

func (l prometheusLabels) Desc(fqname string) *prometheus.Desc {
	return prometheus.NewDesc(fqname, "", l.Names, nil)
}
