package config_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"reflect"
	"testing"

	"gitlab.com/gl-infra/prometheus-git-exporter/config"
)

func TestValidConfiguration(t *testing.T) {
	tests := []struct {
		name     string
		golden   string
		expected config.Config
	}{
		{
			name:   "Given a simple repository it loads the configuration",
			golden: "valid-simple.yml",
			expected: config.Config{
				Basepath: "/tmp",
				Timeout:  10,
				Repositories: []*config.Repository{
					&config.Repository{
						Name:      "simple",
						RemoteURL: "localhost:9091",
					},
				},
			},
		},
		{
			name:   "Given a completely configured repository it loads the configuration",
			golden: "valid.yml",
			expected: config.Config{
				Basepath:          "/tmp",
				Timeout:           100,
				Housekeeping:      10,
				AllowedOperations: 100,
				Repositories: []*config.Repository{
					&config.Repository{
						Name:         "local",
						RemoteURL:    "localhost:9090",
						ResetCommits: 100,
						Labels:       map[string]string{"tier": "blackbox", "env": "prd"},
						UsernameFile: "test-fixtures/repo.username",
						Username:     "username",
						PasswordFile: "test-fixtures/repo.password",
						Password:     "XXXXXXXX",
					},
					&config.Repository{
						RemoteURL: "otherhost:9091",
						Name:      "other",
					},
				},
			},
		},
		{
			name:   "Given a completely configured repository with a git url it loads the configuration",
			golden: "valid-git.yml",
			expected: config.Config{
				Basepath: "/tmp",
				Timeout:  10,
				Repositories: []*config.Repository{
					&config.Repository{
						Name:      "git",
						RemoteURL: "git@gitlab.com:my-repo.git",
						Labels:    map[string]string{"tier": "blackbox", "env": "prd"},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := config.LoadConfiguration(path.Join("./test-fixtures", tt.golden), false)
			if err != nil {
				t.Fatal(err)
			}
			assertEquals(t, "Configured Basepath ", tt.expected.Basepath, c.Basepath)
			assertEquals(t, "Configured Timeout", tt.expected.Timeout, c.Timeout)
			assertEquals(t, "Allowed operations", tt.expected.AllowedOperations, c.AllowedOperations)
			assertEquals(t, "Repositories count", len(tt.expected.Repositories), len(c.Repositories))
			for i, expected := range tt.expected.Repositories {
				actual := c.Repositories[i]
				assertEquals(t, "Repository name", expected.Name, actual.Name)
				assertEquals(t, "Repository reset commits", expected.ResetCommits, actual.ResetCommits)
				assertEquals(t, "Repository remote url", expected.RemoteURL, actual.RemoteURL)
				assertEquals(t, "Repository labels", expected.Labels, actual.Labels)
				assertEquals(t, "Repository username file", expected.UsernameFile, actual.UsernameFile)
				assertEquals(t, "Repository password file", expected.PasswordFile, actual.PasswordFile)
				_, err := actual.GetOriginURL()
				if err != nil {
					t.Fatalf("Get Origin URL failed with error: %s", err)
				}
			}
		})
	}
}

func TestLoadConfigurationWithInvalidFileFails(t *testing.T) {
	f, err := ioutil.TempFile(os.TempDir(), "config")
	if err != nil {
		t.Fatal(err)
	}
	f.Close()
	os.Remove(f.Name())

	if _, err := config.LoadConfiguration(f.Name(), false); err != nil {
		assertEquals(t, fmt.Sprint("unexpected error ", err),
			fmt.Sprintf("open %s: no such file or directory", f.Name()), fmt.Sprint(err))
	}
}

func TestLoadConfigurationWithInvalidFile(t *testing.T) {
	tests := []struct {
		name     string
		golden   string
		expected string
	}{
		{
			name:     "empty configuration",
			golden:   "empty.yml",
			expected: "basepath is a required field",
		},
		{
			name:     "invalid basepath",
			golden:   "non-existing-basepath.yml",
			expected: "basepath is invalid: stat /random-non-existing-folder: no such file or directory",
		},
		{
			name:     "configuration without repositories",
			golden:   "no-repos.yml",
			expected: "there aren't any repository configured",
		},
		{
			name:     "repository without name",
			golden:   "no-repo-name.yml",
			expected: "name is a required repository field",
		},
		{
			name:     "repository without a remote url",
			golden:   "no-url.yml",
			expected: "remote_url is a required repository field",
		},
		{
			name:     "repository with invalid username",
			golden:   "invalid-username.yml",
			expected: "failed to read username for repo local: the secret from file test-fixtures/empty.username is empty",
		},
		{
			name:     "repository pointing to non existing username file",
			golden:   "non-existing-username.yml",
			expected: "failed to read username for repo local: could not read secret from file test-fixtures/non-existing-username: open test-fixtures/non-existing-username: no such file or directory",
		},
		{
			name:     "repository with invalid password",
			golden:   "invalid-password.yml",
			expected: "failed to read password for repo local: the secret from file test-fixtures/empty.password is empty",
		},
		{
			name:     "repository pointing to non existing password file",
			golden:   "non-existing-password.yml",
			expected: "failed to read password for repo local: could not read secret from file test-fixtures/non-existing-password: open test-fixtures/non-existing-password: no such file or directory",
		},
		{
			name:     "configuration file that is not a real yaml",
			golden:   "broken-config.yml",
			expected: "yaml: unmarshal errors:\n  line 1: cannot unmarshal !!str `this is...` into config.Config",
		},
		{
			name:     "repository with invalid kill threshold value",
			golden:   "invalid-allowed-operations.yml",
			expected: "yaml: unmarshal errors:\n  line 5: cannot unmarshal !!str `this-is...` into int",
		},
		{
			name:     "repository with invalid reset commits value",
			golden:   "invalid-reset-commits.yml",
			expected: "yaml: unmarshal errors:\n  line 8: cannot unmarshal !!str `this-is...` into int",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := config.LoadConfiguration(path.Join("./test-fixtures", tt.golden), false)
			assertEquals(t, "Error", tt.expected, fmt.Sprint(err))
		})
	}
}

func TestSecretsAreWiped(t *testing.T) {
	usernameFile := "/tmp/my-secret-username-file"
	passwordFile := "/tmp/my-secret-password-file"

	if err := ioutil.WriteFile(usernameFile, []byte("myusername"), 0644); err != nil {
		t.Fatalf("Could not create username file: %s", err)
	}
	if err := ioutil.WriteFile(passwordFile, []byte("mypassword"), 0644); err != nil {
		t.Fatalf("Could not create password file: %s", err)
	}

	config, err := config.LoadConfiguration(path.Join("./test-fixtures", "valid-with-tmp-secret.yml"), true)
	if err != nil {
		t.Fatalf("Could not load configuration: %s", err)
	}
	for _, r := range config.Repositories {
		assertEquals(t, "password", "mypassword", r.Password)
		assertEquals(t, "username", "myusername", r.Username)
	}

	_, err = os.Stat(usernameFile)
	assertEquals(t, "username file", "stat /tmp/my-secret-username-file: no such file or directory", fmt.Sprint(err))

	_, err = os.Stat(passwordFile)
	assertEquals(t, "password file", "stat /tmp/my-secret-password-file: no such file or directory", fmt.Sprint(err))
}

func assertEquals(t *testing.T, message string, expected, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("%s expected: %v got: %v", message, expected, actual)
	}
}
