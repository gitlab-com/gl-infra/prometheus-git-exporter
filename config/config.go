package config

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

// Config is the top-level configuration for the exporter
type Config struct {
	AllowedOperations int           `yaml:"allowed_operations,omitempty"`
	Basepath          string        `yaml:"basepath"`
	Housekeeping      int           `yaml:"housekeeping"`
	Repositories      []*Repository `yaml:"repositories,omitempty"`
	Timeout           int           `yaml:"timeout_seconds,omitempty"`
}

// Repository is a single repository configuration
type Repository struct {
	Labels       map[string]string `yaml:"labels,omitempty"`
	Name         string            `yaml:"name"`
	Password     string
	PasswordFile string `yaml:"password_file,omitempty"`
	RemoteURL    string `yaml:"remote_url"`
	ResetCommits int    `yaml:"reset_commits,omitempty"`
	Username     string
	UsernameFile string `yaml:"username_file,omitempty"`
}

// GetOriginURL builds the url based on the remote source url plus credentials
func (r Repository) GetOriginURL() (string, error) {
	if strings.HasPrefix(r.RemoteURL, "git@") { // It is a git url, we don't need any secrets loading
		return r.RemoteURL, nil
	}

	u, err := url.Parse(r.RemoteURL)
	if err != nil {
		return "", fmt.Errorf("could parse remote url: %s", err)
	}
	if r.Username != "" {
		u.User = url.UserPassword(r.Username, r.Password)
	}
	return u.String(), nil
}

// LoadConfiguration searches the provided paths for configuration files and loads it
func LoadConfiguration(filename string, wipeSecrets bool) (*Config, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	c := &Config{Timeout: 10, Housekeeping: 10}
	if err := yaml.Unmarshal(content, c); err != nil {
		return nil, err
	}
	if err := c.validate(); err != nil {
		return nil, err
	}
	wiper := newSecretsWiper()
	if wipeSecrets {
		defer wiper.Wipe()
	}

	for _, repo := range c.Repositories {
		if repo.UsernameFile != "" {
			username, err := readSecret(repo.UsernameFile)
			if err != nil {
				return nil, fmt.Errorf("failed to read username for repo %s: %s", repo.Name, err)
			}
			repo.Username = username
			wiper.Add(repo.UsernameFile)
		}

		if repo.PasswordFile != "" {
			password, err := readSecret(repo.PasswordFile)
			if err != nil {
				return nil, fmt.Errorf("failed to read password for repo %s: %s", repo.Name, err)
			}
			repo.Password = password
			wiper.Add(repo.PasswordFile)
		}
	}

	return c, nil
}

func readSecret(secretsFile string) (string, error) {
	secretBytes, err := ioutil.ReadFile(secretsFile)
	if err != nil {
		return "", fmt.Errorf("could not read secret from file %s: %s", secretsFile, err)
	}
	secret := strings.TrimSpace(string(secretBytes))
	if secret == "" {
		return "", fmt.Errorf("the secret from file %s is empty", secretsFile)
	}
	return secret, nil
}

func (c *Config) validate() error {
	if c.Basepath == "" {
		return fmt.Errorf("basepath is a required field")
	}

	fileInfo, err := os.Stat(c.Basepath)
	if err != nil {
		return fmt.Errorf("basepath is invalid: %s", err)
	}
	if !fileInfo.IsDir() {
		return fmt.Errorf("basepath is not a directory")
	}
	if len(c.Repositories) == 0 {
		return fmt.Errorf("there aren't any repository configured")
	}
	for _, r := range c.Repositories {
		if r.Name == "" {
			return fmt.Errorf("name is a required repository field")
		}
		if r.RemoteURL == "" {
			return fmt.Errorf("remote_url is a required repository field")
		}
	}

	return nil
}

type secretsWiper struct {
	files map[string]interface{}
}

func newSecretsWiper() secretsWiper {
	return secretsWiper{
		files: make(map[string]interface{}, 0),
	}
}

func (s secretsWiper) Add(filename string) {
	s.files[filename] = true
}

func (s secretsWiper) Wipe() error {
	for filename := range s.files {
		if err := os.Remove(filename); err != nil {
			return err
		}
	}
	return nil
}
