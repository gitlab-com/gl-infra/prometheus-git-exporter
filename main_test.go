package main

// func TestMeasuring(t *testing.T) {
// 	g := NewTestRepo()
// 	defer func() {
// 		err := g.destroy()
// 		if err != nil {
// 			t.Fatal(err)
// 		}
// 	}()

// 	filename, _ := createTempFile("timeout_seconds: 60")
// 	server := NewServer(":9666", "/metrics", filename)
// 	go server.ListenAndServe()
// 	defer server.Shutdown(nil)

// 	tests := []struct {
// 		name         string
// 		expectedText string
// 	}{
// 		{"Test the server listens", "go_memstats_sys_bytes"},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			// do something
// 			resp, err := http.Get("http://localhost:9666/metrics")
// 			if err != nil {
// 				t.Fatal(err)
// 			}
// 			if resp.StatusCode != 200 {
// 				t.Error("Request failed with error", resp.StatusCode)
// 			}
// 			defer resp.Body.Close()
// 			body, err := ioutil.ReadAll(resp.Body)
// 			if err != nil {
// 				t.Error(err)
// 			}
// 			if !strings.Contains(string(body), tt.expectedText) {
// 				t.Error("Expected text", tt.expectedText, "could not be found in %s", string(body))
// 			}
// 		})
// 	}
// }
