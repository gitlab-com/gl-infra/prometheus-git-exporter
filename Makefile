src = $(wildcard *.go)
executable-linux  = prometheus-git-exporter
executable-darwin = prometheus-git-exporter-darwin
executable-arm    = prometheus-git-exporter-arm64
kubectl-src       = https://storage.googleapis.com/kubernetes-release/release/v1.8.2/bin/linux/amd64/kubectl

.PHONY: all deploy

all: prepare test build package release deploy

validate:
	if [ -z "$(CI_PROJECT_DIR)" ]; then echo "CI_PROJECT_DIR env var is not defined" && exit 1; fi

prepare: validate
	$(eval GOPATH := $(shell mktemp -d))
	$(eval PROJECT := $(shell basename $(CI_PROJECT_DIR)))
	$(eval GROUP := $(shell basename $(shell dirname $(CI_PROJECT_DIR))))
	$(eval WORKDIR := $(GOPATH)/src/gitlab.com/$(GROUP)/$(PROJECT))
	$(eval PATH := $(GOPATH)/bin:$(PATH))
	mkdir -p $(WORKDIR)
	cp -R * $(WORKDIR)
	go get -u github.com/golang/dep/cmd/dep
	cd $(WORKDIR) && dep ensure

test: prepare
	cd $(WORKDIR) && go vet -v
	cd $(WORKDIR) && go test -v ./...

build: prepare
	cd $(WORKDIR) && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags '-w' -o $(executable-linux)
	cp $(WORKDIR)/$(executable-linux) .
	cd $(WORKDIR) && CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=6 go build -a -ldflags '-w' -o $(executable-arm)
	cp $(WORKDIR)/$(executable-arm) .
	cd $(WORKDIR) && CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -a -ldflags '-w' -o $(executable-darwin)
	cp $(WORKDIR)/$(executable-darwin) .

package:
	if [ -z "$(CI_REGISTRY_IMAGE)" ]; then echo "CI_REGISTRY_IMAGE env var is not defined" && exit 1; fi
	docker build . -t $(CI_REGISTRY_IMAGE):latest

release: package
	docker push $(CI_REGISTRY_IMAGE):latest
	# If we are pushing tags, tag the built image                                
	if [ ! -z "$(CI_COMMIT_TAG)" ]; then                                            \
		docker tag $(CI_REGISTRY_IMAGE):latest $(CI_REGISTRY_IMAGE):$(CI_COMMIT_TAG); \
		docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_TAG);                            \
	fi

deploy:
	if [ -z "$$KUBERNETES_CONFIG" ]; then \
		echo "KUBERNETES_CONFIG env var is not defined" && \
		exit 1; \
	fi
	wget $(kubectl-src) -O /usr/local/bin/kubectl
	chmod +x /usr/local/bin/kubectl
	mkdir /root/.kube
	echo "$$KUBERNETES_CONFIG" > /root/.kube/config
	# if we are pushing tags, deploy
	if [ ! -z "$(CI_COMMIT_TAG)" ]; then \
	  sed -i "s/registry.gitlab.com\/gl-infra\/prometheus-git-exporter:latest/registry.gitlab.com\/gl-infra\/prometheus-git-exporter:$(CI_COMMIT_TAG)/" $(CI_PROJECT_DIR)/kubernetes.yaml ; \
	fi
	/usr/local/bin/kubectl apply -f $(CI_PROJECT_DIR)/kubernetes.yaml

