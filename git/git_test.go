package git_test

import (
	"testing"

	"gitlab.com/gl-infra/prometheus-git-exporter/git/testutil"
)

func TestPullWorks(t *testing.T) {
	o := testutil.NewTestRepo("testing-repo-1")
	defer o.Destroy()

	s := o.Source()
	if err := s.Commit("something"); err != nil {
		t.Errorf("Commit failed with error: %s", err)
	}

	r := o.Clone()
	if err := r.Pull(); err != nil {
		t.Errorf("Pull failed with error: %s", err)
	}
}

func TestCommitAndThenPushWorks(t *testing.T) {
	o := testutil.NewTestRepo("testing-repo-2")
	defer o.Destroy()

	r := o.Clone()
	if err := r.Commit("nothing to declare"); err != nil {
		t.Errorf("Commit failed with error: %s", err)
	}

	if err := r.Push(); err != nil {
		t.Errorf("Push failed with: %s", err)
	}
}
