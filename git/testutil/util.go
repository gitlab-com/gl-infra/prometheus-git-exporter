package testutil

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"sync"

	"gitlab.com/gl-infra/prometheus-git-exporter/config"
	"gitlab.com/gl-infra/prometheus-git-exporter/git"
)

type TestRepo struct {
	Basepath     string
	i            int
	m            sync.Mutex
	Origin       string
	resetCommits int
	Timeout      int
}

func (g *TestRepo) Source() *git.Repo {
	r := git.NewRepo(g.Origin, g.Timeout, g.resetCommits)
	return r
}

func (g *TestRepo) nextRepoPath() string {
	g.m.Lock()
	defer g.m.Unlock()

	g.i = g.i + 1
	return path.Join(g.Basepath, fmt.Sprintf("repo-%d", g.i))
}

func (g *TestRepo) Clone() *git.Repo {
	s := g.Source()
	r := git.NewRepo(g.nextRepoPath(), g.Timeout, g.resetCommits)

	if err := r.Clone(s.Path); err != nil {
		panic(err)
	}
	return r
}

func NewTestRepo(repoName string) *TestRepo {
	cdir, _ := os.Getwd()
	defer os.Chdir(cdir)

	dir, err := ioutil.TempDir("", "git-exporter")
	if err != nil {
		panic(fmt.Sprintf("Could not create temporary dir %s", err))
	}

	cmd := exec.Command("git", "init", "source-repo")
	cmd.Dir = dir
	if out, err := cmd.CombinedOutput(); err != nil {
		panic(fmt.Sprintf("Could not init the source git repo %s with output %s", err, out))
	}

	r := &TestRepo{
		Basepath:     dir,
		Timeout:      10,
		Origin:       path.Join(dir, "source-repo"),
		resetCommits: 10,
	}

	o := r.Source()
	for i := 0; i < 10; i++ {
		if err := o.Commit("Beep"); err != nil {
			panic(fmt.Sprintf("Could not commit to repo %s %s", o.Path, err))
		}
	}

	os.Chdir(o.Path)
	cmd = exec.Command("git", "config", "--local", "receive.denyCurrentBranch", "ignore")
	if err := cmd.Run(); err != nil {
		panic(fmt.Sprint("Could not configure local repo", err))
	}

	if err := git.CloneAll(config.Config{
		Timeout:  r.Timeout,
		Basepath: r.Basepath,
		Repositories: []*config.Repository{
			&config.Repository{
				Name:      repoName,
				RemoteURL: r.Origin,
			},
		},
	}); err != nil {
		panic(err)
	}

	return r
}

func (g *TestRepo) Destroy() error {
	return os.RemoveAll(g.Basepath)
}
