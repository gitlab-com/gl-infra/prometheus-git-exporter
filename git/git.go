package git

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/gl-infra/prometheus-git-exporter/config"
)

// Repo provides functionallity to clone, commit, push and pull from a repo
type Repo struct {
	cmd          *exec.Cmd
	m            *sync.Mutex
	Path         string
	resetCommits int
	timeout      int
}

// NewRepo creates a new git repo with default author name and email if not
// provided
func NewRepo(path string, timeout int, resetCommits int) *Repo {
	return &Repo{
		m:            &sync.Mutex{},
		Path:         path,
		resetCommits: resetCommits,
		timeout:      timeout,
	}
}

// Stringer implements stringer interface
func (g *Repo) String() string {
	return fmt.Sprintf("path: %s", g.Path)
}

// Commit creates a new empty commit with the provided message in the repo
func (g *Repo) Commit(message string) error {
	if message == "" {
		return fmt.Errorf("The commit message is missing")
	}
	log.Debugf("Committing to %s", g.Path)
	_, err := g.run("git", "commit", "--allow-empty", "-m", message)
	return err
}

// Pull updates the current repo by pulling from the remote origin
func (g *Repo) Pull() error {
	log.Debugf("Pulling %s from origin master", g.Path)
	_, err := g.run("git", "pull", "--rebase", "origin", "master")
	return err
}

// Push sends the local commits to the remote origin
func (g *Repo) Push() error {
	log.Debugf("Pushing %s to origin master", g.Path)
	_, err := g.run("git", "push", "origin", "master")
	return err
}

// GC executes a garbage collection to make the repo more compact
func (g *Repo) GC() error {
	log.Debugf("Running garbage collection on %s", g.Path)
	_, err := g.run("git", "gc")
	if err == nil {
		log.Infof("Successfully garbage collected %s", g.Path)
	}
	return err
}

// Reset rewinds the repo to a certain commit in order to keep a short log
func (g *Repo) Reset() error {
	if g.resetCommits < 1 {
		return nil
	}

	log.Debugf("Resetting the repository to commit number %d on %s", g.Path)
	out, err := g.run("git", "rev-list", "HEAD")
	if err != nil {
		return fmt.Errorf("Failed to fetch the target commit to reset for  %s. Error: %s",
			g.Path, err)
	}
	commits := strings.Split(out, "\n")
	targetCommit := commits[len(commits)-10]

	log.Debugf("Running 'git reset --hard %s' on %s", targetCommit, g.Path)
	if _, err := g.run("git", "reset", "--hard", targetCommit); err != nil {
		return fmt.Errorf("Failed to reset %s to commit %s. Error: %s",
			g.Path, targetCommit, err)
	}

	log.Debugf("Force-pushing to origin for %s", g.Path)
	_, err = g.run("git", "push", "--force", "origin", "master")
	if err == nil {
		log.Infof("Successfully reset %s to %d commits after the initial one. SHA: %s",
			g.Path, g.resetCommits, targetCommit)
	}
	return err
}

// Clone makes a new clone of the origin repo in the provided path
func (g *Repo) Clone(origin string) error {
	if g.Local() {
		return nil
	}
	_, err := g.run("git", "clone", origin, g.Path)
	return err
}

// Local returns a boolean indicating if the repository is locally cloned
func (g *Repo) Local() bool {
	_, err := os.Stat(g.Path)
	if err != nil {
		return false
	}
	return true
}

func (g *Repo) run(cmd string, args ...string) (string, error) {
	g.m.Lock()
	defer g.m.Unlock()

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(g.timeout)*time.Second)
	defer cancel()

	log.Debugf("Executing 'git %v' with a context timeout of %d seconds", strings.Join(args, " "), g.timeout)
	g.cmd = exec.CommandContext(ctx, cmd, args...)

	if _, err := os.Stat(g.Path); !os.IsNotExist(err) {
		g.cmd.Dir = g.Path
	}
	var b bytes.Buffer
	g.cmd.Stdout = &b
	g.cmd.Stderr = &b

	if err := g.cmd.Start(); err != nil {
		return b.String(), fmt.Errorf("command %v failed to start with %s", g.cmd, err)
	}

	if err := g.cmd.Wait(); err != nil {
		log.Debugf("Failed to execute 'git %v' within %d seconds", strings.Join(args, " "), g.timeout)
		return b.String(), err
	}

	log.Debugf("Successfully executed 'git %v' within %d seconds", strings.Join(args, " "), g.timeout)
	g.cmd = nil

	return b.String(), nil
}

// CloneAll clones all the repos as an initial step
func CloneAll(c config.Config) error {
	wg := sync.WaitGroup{}
	wg.Add(len(c.Repositories))

	errors := make([]string, 0)

	log.Debugf("Cloning %d repositories locally", len(c.Repositories))

	for _, r := range c.Repositories {
		go func(r config.Repository) {
			defer wg.Done()

			repo := NewRepo(path.Join(c.Basepath, r.Name), c.Timeout, r.ResetCommits)
			origin, err := r.GetOriginURL()
			if err != nil {
				errors = append(errors, fmt.Sprintf("Failed to get origin url for repo %s: %s", r.Name, err))
				return
			}
			if err = repo.Clone(origin); err != nil {
				errors = append(errors, fmt.Sprintf("Failed to clone repo %s: %s", r.Name, err))
				return
			}
			log.Debugf("Cloned %s into %s", r.Name, repo.Path)
		}(*r)
	}
	wg.Wait()
	if len(errors) > 0 {
		return fmt.Errorf("could not prepare repositories for collection: %s",
			strings.Join(errors, ", "))
	}
	return nil
}
