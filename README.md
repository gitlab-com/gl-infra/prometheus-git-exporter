# Prometheus git blackbox exporter

A Prometheus exporter that measures the time it takes to pull and push from
remote git repos. It supports both HTTPS and SSH transports.

## Configuration and execution

```yaml
basepath: /where/to/store/repositories
timeout_seconds: 10
repositories:
  - remote_url: url_to_git_repo
    name: repository_name
    username_file: file-containing-the-username
    password_file: file-containing-the-password
    labels:
      tier: blackbox
      environment: test
```

Username and password can be omitted when using SSH keys to authenticate.

Timeout is expressed in seconds, defaults is 10.

### Other useful configuration options

#### `housekeeping`

Scope: global

Default: none

How many operations before a housekeeping cycle starts. This currently only performs a `git gc` on all repositories before going back to serving requests.

#### `allowed_operations`

Scope: global

Default: unlimited

How many operations are allowed to be served before the exporter exists gracefully.

#### `reset_commits`

Scope: repository

Default: disabled

This works in pair with `housekeeping`. When this is set every housekeeping cycle performs a hard git reset to the n-th commit after the first one. This is useful to keep the size of the repository small enough to ensure bloat doesn't interfere with the metrics being reported.

## Arguments

* `-listen-address` the address on which to listen, 9666 by default
* `-metrics-path` path in which to expose metrics, `/metrics` by default
* `-config` the configuration file to use, `.git_exporter` by default
* `-debug` enable debug mode, disabled by default

## Caveats

* The remote url needs to be both readable and writable because this exporter
will probe both pull and push.
* Remember to specify Linux as the platform if you're building the binary on
Mac OS: `CGO_ENABLED=0 GOOS=linux go build -a -tags netgo -ldflags '-w' .`

## Running on Kubernetes

The kubernetes.yaml file contains a sample configuration to run the exporter
on Kubernetes.
