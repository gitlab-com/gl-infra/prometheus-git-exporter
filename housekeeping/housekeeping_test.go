package housekeeping_test

import (
	"testing"

	"gitlab.com/gl-infra/prometheus-git-exporter/housekeeping"
)

func TestHousekeeping(t *testing.T) {
	h := housekeeping.New()
	h.Set(10)
	h.Add("one")
	counter := 0
	for i := 0; i < 20; i++ {
		if err := h.Run("one", func() error {
			counter++
			return nil
		}); err != nil {
			t.Fatal(err)
		}
	}
	if counter != 2 {
		t.Fatalf("counter should be 2, but it is %d instead", counter)
	}
}
