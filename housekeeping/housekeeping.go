package housekeeping

import (
	"fmt"
	"sync"
)

// Housekeeper is a struct that keeps a map of counters and a countdown. Every
// time we invoke Run we will decrease the corresponding counter, when it hits 0
// we will In fact invoke the housekeeping function that is passed as an
// argument to the Run function
type Housekeeper struct {
	initial  int
	counters map[string]int
	m        *sync.Mutex
}

// NewHousekeeper creates a new house keeping object
func New() *Housekeeper {
	return &Housekeeper{
		initial:  0,
		counters: make(map[string]int, 0),
		m:        &sync.Mutex{},
	}
}

// Set sets the amount of times Run has to be called to execute the housekeeping function.
func (h *Housekeeper) Set(initial int) {
	h.initial = initial
}

// Add adds a named counter to keep track of
func (h *Housekeeper) Add(name string) {
	h.counters[name] = h.initial
}

// Run decreases the housekeeping counter and executes the housekeeping function when the counter
// reaches 0. At the same time, when the counter reaches 0 the counter goes up to the initial value
func (h *Housekeeper) Run(name string, f func() error) error {
	shouldHousekeep, err := h.dec(name)
	if err != nil {
		return fmt.Errorf("could not run housekeeping for %s: %s", name, err)
	}
	if shouldHousekeep {
		return f()
	}
	return nil
}

func (h *Housekeeper) dec(name string) (bool, error) {
	h.m.Lock()
	defer h.m.Unlock()

	cur, ok := h.counters[name]
	if !ok {
		return false, fmt.Errorf("could not find repo %s in map", name)
	}
	cur--
	if cur > 0 {
		h.counters[name] = cur
		return false, nil
	}
	h.counters[name] = h.initial
	return true, nil
}
