package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gl-infra/prometheus-git-exporter/collector"
	"gitlab.com/gl-infra/prometheus-git-exporter/config"
	"gitlab.com/gl-infra/prometheus-git-exporter/git"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	addr := flag.String("listen-address", ":9666", "the address on which to listen")
	metricsPath := flag.String("metrics-path", "/metrics", "path in which to expose metrics")
	configfile := flag.String("config", ".git_exporter", "the configuration file to use")
	wipeSecrets := flag.Bool("wipe-secrets", false, "wipe secrets files after loading the configuration")
	debug := flag.Bool("debug", false, "enable debug mode")
	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	config, err := config.LoadConfiguration(*configfile, *wipeSecrets)
	if err != nil {
		fmt.Printf("Could not load configuration %s\n\n", err)
		flag.Usage()
		os.Exit(1)
	}

	if err := git.CloneAll(*config); err != nil {
		fmt.Printf("Could not setup the repositories baseline: %s", err)
		os.Exit(1)
	}

	server := NewServer(*addr, *metricsPath, *config)
	go func() {
		err := server.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()
	log.Info("Started HTTP server on address ", *addr)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

	log.Info("Received interrupt signal, shutting down gracefully")
	cancel()
	<-ctx.Done()
	if err := server.Shutdown(ctx); err != nil && err != context.Canceled {
		log.Println(err)
	}
	log.Info("Shutdown completed successfully. Bye bye!")
}

// NewServer returns a new http server that is setup to collect repositories metrics
func NewServer(addr, metricsPath string, c config.Config) *http.Server {
	server := http.Server{Addr: addr}
	http.Handle(metricsPath, promhttp.Handler())
	prometheus.Register(collector.New(c))
	return &server
}
